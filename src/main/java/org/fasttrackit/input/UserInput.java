package org.fasttrackit.input;

import org.fasttrackit.ui.Display;
import org.fasttrackit.user.BankCustomer;

import java.io.InputStream;
import java.util.Scanner;

public class UserInput {

    public static String readFromKeyBoard() {
        InputStream in = System.in;
        Scanner keyboard = new Scanner(in);
        return keyboard.next();
    }

    public static boolean authenticate(BankCustomer someOne, Display ui) {
        for (int i = 0; i < 3; i++) {
            String pinCode = ui.askForPin();
            boolean isValid = someOne.validatePin(pinCode);
            if (isValid) {
                return true;
            }
            ui.displayInvalidPinMsg();

        }
        return false;
    }


}
