package org.fasttrackit.ui;

import org.fasttrackit.input.UserInput;
import org.fasttrackit.user.BankAccount;
import org.fasttrackit.user.BankCustomer;

import static org.fasttrackit.AtmApp.ATM_NAME;

public class Display {
    public static final String APP_MENU =
            """
            Select an option:
            1. Withdraw cash
            2. Check balance
            3. Change pin
            4. Activate card
            5. Deposit cash
            6. Deactivate account
            7. Show IBAN
            """;

    public static void showIban(BankCustomer someOne) {
        System.out.println("IBAN: " + someOne.getBankAccount().getAccountNumber());

    }

    public void showWelcomeMsg() {
        System.out.println("Welcome to " + ATM_NAME);
    }

    public String askForPin() {
        System.out.println("Please enter your pin number: ");
        return UserInput.readFromKeyBoard();
    }

    public void displayInvalidPinMsg() {
        System.out.println("Pin incorrect, please try again.");
    }

    public void lockUserFor24h(){
        System.out.println("Incorrect pin, card locked for 24h.");
    }
    public void showMenu() {
        System.out.println(APP_MENU);
    }

    public void askForAmount() {
        System.out.println("Type in the amount you want to withdraw : ");
    }

    public void checkBalance() {
        System.out.println("Check balance is selected.");
    }

    public void showCustomerBalanceSheet(BankAccount bankAccount) {
        System.out.println("Your Balance is : " + bankAccount.getBalance() + " " + bankAccount.getCurrency());
    }

    public void showNotEnoughMoneyMsg() {
        System.out.println("Your bank account does not have enough cash.");
    }
}
