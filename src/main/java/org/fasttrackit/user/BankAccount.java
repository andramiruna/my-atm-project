package org.fasttrackit.user;

public class BankAccount {
    private final String accountNumber;
    private final String currency;
    public String getAccountNumber() {
        return accountNumber;
    }
    private double balance = 0;
    private Card card;

    public BankAccount(String accountNumber, String currency) {
        this.accountNumber = accountNumber;
        this.currency = currency;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCard(Card card) {
        this.card = card;
    }

    public Card getCard() {
        return card;
    }

    public double getBalance() {
        return balance;
    }

    public void updateBalance(double balance) {
        this.balance = balance;
    }
}
