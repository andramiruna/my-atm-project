package org.fasttrackit.user;

import org.fasttrackit.input.UserInput;

public class BankCustomer {
    private final int id;
    private final String personalIdnumber;
    private final String firstName;
    private final String lastName;
    private String middleName;

    private BankAccount bankAccount;

    public BankCustomer(int bId, String cnp, String firstName, String lastName) {
        this.id = bId;
        this.personalIdnumber = cnp;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public BankAccount getBankAccount() {
        return bankAccount;
    }

    public void setAccount(BankAccount bankAccount) {
        this.bankAccount = bankAccount;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public boolean validatePin(String input){
        System.out.println("Person : " + firstName + " typed : " + input);
        return bankAccount.getCard().verifyPin(input);

    }
    public String selectOptionMenu() {
        String option = UserInput.readFromKeyBoard();
        System.out.println("Person : " + firstName + " selected " + option);
        return option;
    }

    public void withdrawCash(String input) {
        if (middleName != null) {
            System.out.println("Person : " + firstName + " " + middleName + " withdraw " + input + " Lei");
            return;
        }
        System.out.println("Person : " + firstName + " " + lastName + " withdraw " + input + " Lei");

    }

    public void checkBalance(String input) {
        if (middleName != null) {
            System.out.println("Person : " + firstName + " " + middleName + " balance is " + input + " lei");
            return;
        }
        System.out.println("Person: " + firstName + " " + lastName + " balance is  " + input + " Lei ");
    }
}
