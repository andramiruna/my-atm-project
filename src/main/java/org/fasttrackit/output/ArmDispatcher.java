package org.fasttrackit.output;

import org.fasttrackit.ui.Display;
import org.fasttrackit.user.BankAccount;

public class ArmDispatcher {
    public static void withdraw(BankAccount account, int amount){
        double balance = account.getBalance();
        Display ui = new Display();
        if (balance < amount) {
            ui.showNotEnoughMoneyMsg();
            return;
        }
        account.updateBalance(balance - amount);


    }
}
