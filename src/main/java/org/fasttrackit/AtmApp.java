package org.fasttrackit;

import org.fasttrackit.output.ArmDispatcher;
import org.fasttrackit.ui.Display;
import org.fasttrackit.user.BankAccount;
import org.fasttrackit.user.BankCustomer;
import org.fasttrackit.user.Card;

import static org.fasttrackit.input.UserInput.authenticate;
import static org.fasttrackit.utils.DataUtils.getCustomer;

public class AtmApp {
    public static final String ATM_NAME = "Free Cash ATM";

    public static void main(String[] args) {
        BankCustomer someOne = getCustomer();

        Display ui = new Display();
        ui.showWelcomeMsg();

        boolean passThrough = authenticate(someOne, ui);
        if (!passThrough) {
            ui.lockUserFor24h();
            return;
        }
        ui.showMenu();
        String option = someOne.selectOptionMenu();
        switch (option){
            case "1" -> ArmDispatcher.withdraw(someOne.getBankAccount(), 100);
            case "2" -> ui.showCustomerBalanceSheet(someOne.getBankAccount());
            case "7" -> Display.showIban(someOne);
        }
//        ui.askForAmount();
//        someOne.withdrawCash("100");


    }


}
