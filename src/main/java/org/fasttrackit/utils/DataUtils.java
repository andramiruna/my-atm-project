package org.fasttrackit.utils;

import org.fasttrackit.user.BankAccount;
import org.fasttrackit.user.BankCustomer;
import org.fasttrackit.user.Card;

public class DataUtils {

    public static BankCustomer getCustomer() {
        BankCustomer customer = new BankCustomer(2, "180010045588414", "Alex", "Danciu");
        BankAccount account = new BankAccount("RO00RCNTN02211455", "RON");
        account.updateBalance(150);
        Card card = new Card("5298759817365198", "3838");
        account.setCard(card);
        customer.setAccount(account);
        return customer;
    }
}
